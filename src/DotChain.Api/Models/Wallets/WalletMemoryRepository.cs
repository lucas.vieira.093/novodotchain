using System.Collections.Generic;

namespace DotChain.Api.Models.Wallets;

public class WalletMemoryRepository: IWalletRepository
{
    private readonly Dictionary<string, Wallet> _dictionary;

    public WalletMemoryRepository()
    {
        _dictionary = new Dictionary<string, Wallet>();
    }

    public void Add(Wallet wallet)
    {
        _dictionary[wallet.PublicAddress] = wallet;
    }
    
    public Wallet? Get(string address)
    {
        return _dictionary.ContainsKey(address) ? _dictionary[address] : null;
    }
}