using System.Diagnostics.CodeAnalysis;

namespace DotChain.Models;

[ExcludeFromCodeCoverage]
public record BlockMessage
{
    public string Address { get; init; } = "";
    public long Timestamp { get; init; }
    public string Message => $"{Address}:{Timestamp}:starRegistry";

    public BlockMessage(string address, long timestamp)
    {
        Address = address;
        Timestamp = timestamp;
    }

    public BlockMessage(string message)
    {
        if (string.IsNullOrEmpty(message)) return;
        var tokens = message.Split(':');
        Address = tokens[0];
        Timestamp = long.Parse(tokens[1]);
    }
}