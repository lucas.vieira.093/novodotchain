using System;
using DotChain;
using FluentAssertions;
using UnitTest.Shared;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest;

public class BlockchainTest : Fixture
{
    public BlockchainTest(ITestOutputHelper outputHelper) : base(outputHelper)
    {
    }    
      
    [Fact]
    public void a_block_chas_should_have_a_genesis_block_when_created()
    {
        Blockchain = new Blockchain();

        Blockchain.Height.Should().Be(1);
        Blockchain.GetBlockByHeight(0).Should().NotBeNull();
        Blockchain.GetBlockByHeight(1).Should().BeNull();
    }
}