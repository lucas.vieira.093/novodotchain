# Avaliação Final
**TÓPICOS ESPECIAIS EM DESENVOLVIMENTO DE SOFTWARE 1 -
Turma: 01 (2022.2)**

### Objetivo
Nesta avaliação o aluno deve evidênciar que assimilou os conhecimentos
necessário para: realizar operações em sistemas de controle de versão git como
criar branches e submeter mudanças para revisão; criar estágios de integração contínua
na pipeline do sistema gitlab contendo as fases de build e testes; criar estágios de delivery, 
no qual é gerada uma imagem (docker) da aplicação. Para isso, o aluno deverá cumprir as 
seguintes atividades:

---

#### 1. Solicitar acesso ao projeto dotchain
- [x] Acesse o [Repositório do dotchain](https://gitlab.com/yan.justino/dotchain) e clique no link "Request Access", como ilustra a imagem abaixo

![img.png](assets/fig01.png)

- [x] Aguarde a liberação do acesso
- [x] Com acesso liberado, faça o clone do projeto pra sua máquina local

#### 2. Criar seu branch de trabalho
- [x] Crie uma branch chamada `fix/{nome}-{sobrenome}` (ex.: `fix/yan-justino`)

#### 3. Crie e publique seu branch de trabalho com um novo arquivo
- [x] Na sua branch de trabalho crie um arquivo chamado `{nome}_{sobrenome}.md` (ex.: `yan_justino.md`).
- [x] Faça o commit e o push da sua branch de trabalho.
- [x] Aguarde a [pipeline](https://gitlab.com/yan.justino/dotchain/-/pipelines?scope=branches&page=1) terminar de executar

#### 4. Corrigir teste falhando
- [x] Após publicar sua branch e a pipeline concluir a execução vc deve perceber que exite uma falha no stage de _Test_.
- [x] Corriga o teste `new_blocks_must_have_only_the_body()` no arquivo `BlockTeste.cs`
- ⚠️ O erro é o seguinte:
```
Expected Block.Height to be 0 because Ao construir o atributo 'Height' deve ser 0, but found 1 (difference of 1).
```
- 🚨 A alteração pra ajustar o teste deve ser feita no arquivo `Block.cs`
- [x] Faça o commit da correção
- A pipeline deve ser rodar sem erros

#### 5. Criar Merge-Request
- Criar Merge request de sua branch de trabalho para `developer`
- 🚨 **NÃO FECHAR O MR** 
- Coloque no assigne o seu usuário
- Coloque no campo reviewer o usuário `yan.justino` 
- Seu MR deve ficar semelhante como abaixo:
  ![img.png](assets/fig02.png)
- ⚠️ Enviar o link do MR no Fórum da avaliação 

### Agenda de atendimento

- 📅 28/07 - das 16h às 19h
- 📅 30/07 - das 09h às 12h