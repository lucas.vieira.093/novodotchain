# Dotchain


[![pipeline status](https://gitlab.com/yan.justino/dotchain/badges/master/pipeline.svg)](https://gitlab.com/yan.justino/dotchain/-/commits/master)

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Este obra está licenciado com uma Licença <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Atribuição-NãoComercial 4.0 Internacional</a>.

### Sobre o uso desse repositório

Esse repositório de código contém artefatos de software utilizados durante as 
aulas da disciplina **TÓPICOS ESPECIAIS EM DESENVOLVIMENTO DE SOFTWARE 1 - 
Turma: 01 (2022.2)**, do INSTITUTO METROPOLE DIGITAL (IMD) / UFRN
. Nesse sentido, os códigos e aplicações da forma como estão aqui registrados devem ser
considerados como material utilizados para fins didáticos. Portanto, não representam uma sugestão ou insumo 
que deva ser aplicada para soluções reais.

### API

O uso da API do dotchain é uma forma didática de apresentar
os conceitos fundamentais da arquitetura de blockchain. Nela 
exitem dois grupos de serviços:

- **Serviços de Wallet:** responsáveis por criar e localizar novas carteiras digitais, bem como, assinar e validar assinaturas digitais que foram produzidas pelo par de chaves da Wallet.
- **Serviços da blockchai**: responsáveis por realizar operações na blockchain como adicionar um objeto na blockchain, criar uma mensagem única para validar uma operação; além de prover consultas de blocos na blockchain utilizando alguns parâmetros de busca!


Para melhor compreensão da API recomendamos a execução do seguinte fluxo:

#### 1. Criar uma wallet 

Request:
```bash
curl -X 'POST' \
  'https://localhost:7082/api/v1/Wallet' \
  -H 'accept: text/plain' \
  -d ''
```
Response (example):
```json
{
  "wallet": {
    "publicAddress": "mzMpZyVxzfsbNFbA2drjJpRaQ5Vrp6CvVi",
    "publicKey": "AmIins05sYbxSeaY8MGhbLOB3XfAFrjiL4WWVpaiKr4Y"
  },
  "result": "Wallet was created!!!"
}
```
▶︎ copiar o atributo `publicAddress` do item 1.



#### 2. Criar uma mensagem de validação da wallet

Request:
```bash
curl -X 'POST' \
  'https://localhost:7082/api/v1/Blockchain/requestValidation' \
  -H 'accept: text/plain' \
  -H 'Content-Type: multipart/form-data' \
  -F 'address={{publicAddress}}'
```

Response (example):
```json
{
  "address": "mzMpZyVxzfsbNFbA2drjJpRaQ5Vrp6CvVi",
  "timestamp": 1658493639,
  "message": "mzMpZyVxzfsbNFbA2drjJpRaQ5Vrp6CvVi:1658493639:starRegistry"
}
```
▶︎ copiar atributo `message` do resultado

#### 3. Assinar mensagem de validação com a carteira

Request:
```bash
curl -X 'POST' \
  'https://localhost:7082/api/v1/Wallet/{{publicAddress}}/sign' \
  -H 'accept: text/plain' \
  -H 'Content-Type: multipart/form-data' \
  -F 'data={{message}}'
```

Response (example):
```json
{
  "wallet": {
    "publicAddress": "mzMpZyVxzfsbNFbA2drjJpRaQ5Vrp6CvVi",
    "publicKey": "AmIins05sYbxSeaY8MGhbLOB3XfAFrjiL4WWVpaiKr4Y"
  },
  "result": "MEQCICY/UJa1d9cSJOmDCkf6qOo0clwewF+KOuHCqTdvlNh0AiB0TZhxVcbMptqWxMyKTze0ZxpnVsL5XK2duocHDVq8rQ=="
}
```
▶︎ copiar atributo `result` desse resultado


#### 4. Adicionar item na blockchain
   1. Valida se a mensagem tem mais de 5min. de criação
   2. Valida a assinatura a partir da mensagem 

Request:
```bash
curl -X 'POST' \
  'https://localhost:7082/api/v1/Blockchain/submitstar' \
  -H 'accept: text/plain' \
  -H 'Content-Type: application/json' \
  -d '{
  "address": "{{publicAddress}}",
  "message": "{{message}}",
  "signature": "{{signature}}",
  "star": {
    "rating": 5,
    "description": "My 5 stars"
  }
}'
```

Response (example):
```json
{
  "hash": "4fe1c6daa5156d53aec9a3504a001046409374d9c9c0a88b1a5767770693a447",
  "height": 1,
  "body": "7b226f776e6572223a226d7a4d705a7956787a6673624e4662413264726a4a70526151355672703643765669222c22626c6f636b53746172223a7b22726174696e67223a352c226465736372697074696f6e223a224d792035207374617273227d7d",
  "time": 1658495383,
  "previousBlockHash": "fe9c5606a4eb2f52dce1705ac6d23bda7bdc68e41df648147c8120d213943178"
}
```

#### 5. Consultar bloco na blockchain por height

Request:
```bash
curl -X 'GET' \
  'https://localhost:7082/api/v1/Blockchain/block/1' \
  -H 'accept: text/plain'
```

Response (example):
```json
{
  "hash": "4fe1c6daa5156d53aec9a3504a001046409374d9c9c0a88b1a5767770693a447",
  "height": 1,
  "body": "7b226f776e6572223a226d7a4d705a7956787a6673624e4662413264726a4a70526151355672703643765669222c22626c6f636b53746172223a7b22726174696e67223a352c226465736372697074696f6e223a224d792035207374617273227d7d",
  "time": 1658495383,
  "previousBlockHash": "fe9c5606a4eb2f52dce1705ac6d23bda7bdc68e41df648147c8120d213943178"
}
```